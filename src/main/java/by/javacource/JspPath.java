package by.javacource;

public final class JspPath {
    public static final String INDEX = "/index.jsp";

    public static final String GROUP = "/WEB-INF/groupInfo.jsp";

    public static final String GROUPSLIST = "/WEB-INF/groupsList.jsp";

    public static final String SCHOOL = "/WEB-INF/schoolInfo.jsp";

    public static final String SIGNON = "/WEB-INF/signOn.jsp";

    public static final String USER = "/WEB-INF/userInfo.jsp";

}
