package by.javacource.servlets;

import by.javacource.DataProcessor;
import by.javacource.JspPath;
import by.javacource.SearchType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");
        List<String> studentInfo = new DataProcessor().getInfo(user, SearchType.STUDENT);
        if (studentInfo == null) {
            throw new NoSuchElementException("Student not found: " + user);
        }
        req.setAttribute("info", studentInfo);
        this.getServletContext().getRequestDispatcher(JspPath.USER).forward(req, resp);
    }
}
