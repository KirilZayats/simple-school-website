package by.javacource.servlets;

import by.javacource.DataProcessor;
import by.javacource.JspPath;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/newuser")
public class NewUser extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("firstName", req.getParameter("firstName"));
        req.setAttribute("secondName", req.getParameter("secondName"));
        this.getServletContext().getRequestDispatcher(JspPath.SIGNON).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        new DataProcessor().createNewUser(process(req, resp));
        this.getServletContext().getRequestDispatcher(JspPath.INDEX).forward(req, resp);
    }

    private Map<String, String> process(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/plain");
        Map<String, String> newUserInfo = new HashMap<>();
        // Get the values of all request parameters
        Enumeration<String> en = request.getParameterNames();
        while (en.hasMoreElements()) {
            // Get the name of the request parameter
            String name = en.nextElement();
            newUserInfo.put(name, request.getParameter(name));
        }
        return newUserInfo;
    }
}
