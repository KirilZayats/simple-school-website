package by.javacource.servlets;

import by.javacource.DataProcessor;
import by.javacource.JspPath;
import by.javacource.SearchType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/group")
public class GroupServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("groupList",new DataProcessor().getInfo(req.getParameter("group"), SearchType.GROUP));
        this.getServletContext().getRequestDispatcher(JspPath.GROUP).forward(req,resp);
    }
}
