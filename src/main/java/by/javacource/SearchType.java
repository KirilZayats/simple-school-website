package by.javacource;

public enum SearchType {
    STUDENT,
    GROUP,
    GROUPSLIST
}
