package by.javacource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataProcessor {
    private static final String PATH = "data/";

    public void createNewUser(Map<String, String> userInfoList) {
        String userName = userInfoList.get("firstName") + " " + userInfoList.get("secondName");
        String userGrade = userInfoList.get("grade");
        String birthday = userInfoList.get("date");
        String age = userInfoList.get("age");
        String mark = userInfoList.get("mark");
        String userBlock = String.join("\n",
                userName,
                birthday,
                age,
                userGrade,
                mark);
        new FileWorks().setFileData(PATH, userName, userGrade, userBlock);
    }

    public List<String> getInfo(String key, SearchType type) {
        return switch (type) {
            case GROUP, GROUPSLIST -> new FileWorks().getFileData(PATH + key.toLowerCase() + "list.txt");
            case STUDENT -> getStudentInfo(new FileWorks().getFileData(PATH + "studbase.txt"), key);
        };
    }

    private List<String> getStudentInfo(List<String> listOfStudents, String key) {
        List<String> studentInfo = new ArrayList<>();
        if (listOfStudents.contains(key)) {
            for (int i = listOfStudents.indexOf(key); !listOfStudents.get(i).contains("/"); i++) {
                studentInfo.add(listOfStudents.get(i));
            }
        }
        return studentInfo;
    }
}
