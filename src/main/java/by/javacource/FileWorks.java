package by.javacource;


import by.javacource.exception.InternalServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

public class FileWorks {
    private static final Logger logger = LoggerFactory.getLogger(FileWorks.class);

    public List<String> getFileData(String path) {

        try (InputStreamReader streamReader = new InputStreamReader(getFileFromResourceAsStream(path), StandardCharsets.UTF_8);
             BufferedReader bufferedReader = new BufferedReader(streamReader)) {
            return bufferedReader.lines().collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            logger.error("Wrong file name.Check the file path: {}", path);
            throw new InternalServerException("Something wrong happens. Please contact system administrator.");
        } catch (IOException e) {
            logger.error("Failed to read file {}", path, e);
            throw new InternalServerException("Something wrong happens. Please contact system administrator.");
        }
    }

    //See https://mkyong.com/java/java-read-a-file-from-resources-folder/
    // get a file from the resources folder
    // works everywhere, IDEA, unit test and JAR file.
    private InputStream getFileFromResourceAsStream(String fileName) {

        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        // the stream holding the file content
        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }

    }

    //It not works. Because if you are building jar/war with some files it can be only read. No wrinig to jar/war.
    //For that purpose recommended to create temp file (see https://mkyong.com/java/how-to-get-the-temporary-file-path-in-java/)
    //As the result in resource foulder you only have some init data for your pages
    public void setFileData(String path, String userName, String userGrade, String userBlock) {

        try (FileWriter fileWriter = new FileWriter(path + "studbase.txt", true)) {
            fileWriter.write('\n' + userBlock + "\n/");
        } catch (IOException e) {
            logger.error("Wrong file name.Check the file path: {}studbase.txt", path);
            logger.error("Failed to write to file {}studbase.txt", path, e);
        }
        try (FileWriter fileWriter2 = new FileWriter(path + userGrade + "list.txt", true)) {
            fileWriter2.write('\n' + userName);
        } catch (IOException e) {
            logger.error("Wrong file name.Check the file path: {}{}list.txt", path, userGrade);
            logger.error("Failed to write to file {}{}list.txt", path, userGrade, e);
        }
    }
}
