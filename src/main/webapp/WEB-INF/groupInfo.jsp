<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.javacource.JspPath" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>

<head>
    <title>${group}</title>
    <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/index.css" type="text/css"/>
</head>
<body>
<table>
    <tr>
        <td>
            <h1> Student list</h1>
            <c:forEach var="user" items="${groupList}">
                <a href="/user?user=${user}">${user}</a>
                <br>
            </c:forEach>
            <button onclick="location.href='${JspPath.INDEX}'">Home</button>
        </td>
        <td>
            <jsp:include page="${JspPath.SCHOOL}"></jsp:include>
        </td>
    </tr>
</table>
</body>
</html>
