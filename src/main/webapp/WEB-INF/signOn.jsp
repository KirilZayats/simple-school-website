<%@ page import="by.javacource.SearchType" %>
<%@ page import="by.javacource.DataProcessor" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Create new profile</title>
    <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/signOn.css" type="text/css"/>
</head>
<body>
<form action="/newuser" method="post">
    <table border="0">
        <tr>
            <td><b>First Name</b></td>
            <td><input type="text" name="firstName"
                       value="${firstName}" size="15"></td>
            <td><b>Second Name</b></td>
            <td><input type="text" name="secondName"
                       value="${secondName}" size="15"></td>
        </tr>
        <tr>
            <td><b>Birthday: Year</b></td>
            <td>
                <input type="date" id="date" name="date"/>
            </td>


        </tr>
        <tr>
            <td><b>Age</b></td>
            <td><input type="number" name="age"
                       value=""></td>
        </tr>
        <tr>
            <td><b>Grade</b></td>
            <td>
                <select name="grade">
                    <%
                        for (String grade : new DataProcessor().getInfo("groups", SearchType.GROUPSLIST)) {
                            out.println("<option value = \"" + grade + "\">" + grade + "</option>");
                        }
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td><b>Average mark</b></td>
            <td><input type="text" name="mark"
                       value=""></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="register"/></td>
        </tr>
    </table>
</form>

</body>
</html>
