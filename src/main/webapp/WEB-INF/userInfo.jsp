<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.javacource.JspPath" %>
<html>
<head>
    <title>${info0} full info</title>
    <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/index.css" type="text/css"/>
</head>
<body>
<table>
    <tr>
        <td>
            <h1>Student name: ${info.get(0)}</h1>
            <h2>Birthday: ${info.get(1)}</h2>
            <h2>Age: ${info.get(2)}</h2>
            <h2>Grade: ${info.get(3).toUpperCase()}</h2>
            <h2>Average mark: ${info.get(4)}</h2>
            <button onclick="location.href='/group?group=${info.get(3)}'">Back to group list</button>
        </td>
        <td>
            <jsp:include page="${JspPath.SCHOOL}"></jsp:include>
        </td>
    </tr>
</table>

</body>
</html>
