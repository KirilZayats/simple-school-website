<%@ page import="by.javacource.SearchType" %>
<%@ page import="by.javacource.DataProcessor" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    pageContext.setAttribute("groups", new DataProcessor().getInfo("groups", SearchType.GROUP));
%>
<html>
<head>
    <title>list of groups</title>
    <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/index.css" type="text/css"/>
</head>

<h1>LIST OF GROUPS</h1>
<c:forEach var="group" items="${groups}">
    <a href="/group?group=${group}">${group} grade </a>
</c:forEach>

</body>
</html>