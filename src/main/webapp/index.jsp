<%@ page import="by.javacource.JspPath" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>School test site</title>
    <link rel="stylesheet" href="resources/index.css" type="text/css"/>

</head>

<body>
<h1>Welcome to our school site!</h1>
<table>
    <table border="0">
    <tr>
        <td>
            <jsp:include page="${JspPath.SCHOOL}"></jsp:include>
        </td>
        <td>
            <form action="/newuser" method="get">
                <h3>Add new student</h3>
                FirstName: <input type="text" name="firstName" value="">
                <br>
                SecondName: <input type="text" name="secondName" value="">
                <br>
                <button type="submit">Sign on</button>

            </form>
        </td>
    </tr>
</table>

<jsp:include page="${JspPath.GROUPSLIST}"></jsp:include>
</body>
</html>
